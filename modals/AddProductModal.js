import React, {Component} from 'react';
import {Platform, StyleSheet,TextInput, Text,TouchableHighlight, View,Dimensions,Image} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Modal from "react-native-modal";

var devwidth= Dimensions.get('window').width;
var devheight=Dimensions.get('window').height
var devwidth= Dimensions.get('window').width;
var devheight=Dimensions.get('window').height
import uuid from 'uuid';

export default class AddProductModal extends Component {

   state={
    isVisible:false,
    Pname:'',
    Prate:-1,
    Pprice:-1,
    Mname:'',
    errMsg:""

   }
    openModal=()=>{
        this.setState({isVisible:true})
    }
 
    AddProduct =()=>{
   var addProduct=true;
      if(!this.state.Pname){
      this.setState({errMsg:"Product Name can't be empty"})
      addProduct=false;
      return false;
      }

      if(!this.state.Prate){
        this.setState({errMsg:"Product price can't be empty"})
        addProduct=false;
        return false;
        }
       else if(isNaN(this.state.Prate)){
        this.setState({errMsg:"Product rating should be valid one"})
        addProduct=false;
        return false;
       }
        else if(parseInt(this.state.Prate) < 0 || parseInt(this.state.Prate) > 5){
            this.setState({errMsg:"Rating should be 0 to 5"})
            addProduct=false;
            return false;
        }

      if(!this.state.Pprice){
        this.setState({errMsg:"Product price can't be empty"})
        addProduct=false;
        return false;
        }
       else if(isNaN(this.state.Pprice)){
        this.setState({errMsg:"Product price should be valid one"})
        addProduct=false;
        return false;
       }
        else if(parseInt(this.state.Pprice) < 0){
            this.setState({errMsg:"Product price should be valid one"})
            addProduct=false;
            return false;
        }
        if(!this.state.Mname){
            this.setState({errMsg:"Merchant Name can't be empty"})
            addProduct=false;
            return false;
            }
             if(addProduct == true)
             {
                this.setState({errMsg:''})
                var newItem=      {
                    "name":this.state.Pname,
                    "price": parseInt(this.state.Pprice),
                    "image":'http://www.tttcdhanbad.org/images/NoImage.png',
                    "rating":parseInt(this.state.Prate),
                    'MerchantType':this.state.Mname,
                    'uuid':uuid()
                }
                this.props.addNewProduct(newItem)
                this.setState({isVisible:false})
             }
             

    }
    render(){
        return(
            <View>
            <Modal 
             isVisible={this.state.isVisible}
             backdropOpacity={0.8}
             animationIn='zoomInDown'
             animationOut='zoomOutUp'
             animationInTiming={400}
             animationOutTiming={600}
             onBackdropPress={()=>{
                 this.setState({isVisible:false})
             }}
             backdropTransitionInTiming={600}
             backdropTransitionOutTiming={600}>
              <View style={{ borderRadius:4,backgroundColor:'white',justifyContent:'center',alignItems:'center',borderColor:'rgba(0,0,0,0.1)',borderRadius:4}}>
                  <View style={{backgroundColor:'#6a94d8',width:'100%',height:30,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontWeight:'700'}}>Add New Product</Text>
                  </View>
                  <Text style={{color:'red'}}> {this.state.errMsg}</Text>
                <TextInput onChangeText={(Pname) => this.setState({Pname})} underlineColorAndroid="#428AF8" style={{height: 40,width:devwidth/2,marginTop:10}} placeholder="ProductName"/>
                <TextInput onChangeText={(Prate) => this.setState({Prate})} underlineColorAndroid="#428AF8" style={{height: 40,width:devwidth/2}} placeholder="Rating out of 5"/>
                <TextInput onChangeText={(Pprice) => this.setState({Pprice})} underlineColorAndroid="#428AF8" style={{height: 40,width:devwidth/2}} placeholder="Price"/>
                <TextInput onChangeText={(Mname) => this.setState({Mname})} underlineColorAndroid="#428AF8" style={{height: 40,width:devwidth/2}} placeholder="MerchantName"/>
                <TouchableHighlight
         style={{ alignItems: 'center',
         backgroundColor: '#DDDDDD',
         padding: 10,width:devwidth/2,margin:10}}
         onPress={this.AddProduct}
        >
         <Text> Add Product </Text>
        </TouchableHighlight>
       
              </View>
            </Modal>
          </View>
        );
    }

}