/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {ScrollView, StyleSheet,FlatList, Text,TouchableOpacity, View,Dimensions,Image} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import ActionButton from 'react-native-action-button';
import ListProduct from './screens/ListProduct'

var devwidth= Dimensions.get('window').width;
var devheight=Dimensions.get('window').height
import uuid from 'uuid';
import { createStackNavigator, createAppContainer } from "react-navigation";


const AppNavigator = createStackNavigator({
  ListProduct: {
    screen: ListProduct
  },
  
});
const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {


componentWillMount(){
 // console.log(this.state.products)
}



  render() {
    return (

         <AppContainer/>    
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  flatview: {
    justifyContent: 'center',
    paddingTop: 30,
    borderRadius: 2,
  },
});
