import React, {Component} from 'react';
import {ScrollView,Platform, StyleSheet,LayoutAnimation,FlatList, Text,TouchableOpacity, View,Dimensions,Image} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import ActionButton from 'react-native-action-button';

import AddProductModal from '../modals/AddProductModal'
var devheight=Dimensions.get('window').height
var devwidth= Dimensions.get('window').width;

import uuid from 'uuid';
var rootAccess;
export default class ListProduct extends Component {
  constructor(props){
    super(props);
    rootAccess=this;
    this.state ={
      rendertype:'products',
      products:[
        {
          MerchantType:'Flipkart',
          Products:[
            {
              "name": "Redmi Note 7",
              "price": 15000,
              "image":'https://www.91-img.com/pictures/132510-v8-xiaomi-redmi-note-7-pro-mobile-phone-large-1.jpg',
              "rating":4,
              'MerchantType':'Flipkart',
               'uuid':uuid()
          },
          {
            "name": "Realme Note 6 pro",
            "price": 12000,
            "image":'https://www.91-img.com/pictures/129556-v10-xiaomi-redmi-note-6-pro-mobile-phone-large-1.jpg',
            "rating":4.5,
            'MerchantType':'Flipkart',
            'uuid':uuid()
          },
          {
            "name": "Realme pro",
            "price": 11500,
            "image":'https://www.91-img.com/pictures/129465-v6-realme-2-pro-mobile-phone-large-1.jpg',
            "rating":4,
            'MerchantType':'Flipkart',
            'uuid':uuid()
          },
          ]
        },{
        MerchantType:'Amazon',
        Products:[
          {
            "name": "Oneplus 6",
            "price": 35000,
            "image":'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEBIVFRUVFxcVEBUQFQ8WFRYVFRUXFhUVFRcYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0lHyUrKy8tLSstLS0tLTAtLy8tLS0tLS8tLS0tLS0vLS8tLS0tLS0tLS8tLS0tLS0tLS0rLf/AABEIARMAtwMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAABAUGBwECAwj/xABREAABAwIBBggIBw0HBQEAAAABAAIDBBEFBgcSITGyEzRBUWFyc7EUIjJxdIGRtEJSU6HBwtEVFyMzQ1RikpOUotLTJESCg7Ph8BYlNWOjw//EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAwEQACAQIEAwYGAgMAAAAAAAAAAQIDEQQhMVESE0EFImGBscEjMnGh0fCR4RRicv/aAAwDAQACEQMRAD8AvFCEIAE0YnlTQ050airgjd8V8sYd+re6rHOll9IS+lpD4ulwTnNc4XeCdMEt5tE6r2te99JujAKPDHhvjFrTtNmNO3oOpJtLUqMW9C+35xMKH98jPVEju5pXM5ysKH97HqiqT3MVIGhPx/4GLR1G75T+FqXHErlS2Lu++fhX5yf2FZ/TWjs6eFD+8P8AVTVp/wDzVIOo3fKn9Vq5mjd8qf1Go4kHKnsXj99XCvl5P3as/prH31sK+Wl/dqz+RUBXU0jPGuHDlOi2484skcZc69nDVsGiNey/J039RVKzIcWsmejPvq4V8tJ+7Vn9NZGdXCvl5P3at/przbCZHnRZt8zbAc5Nk5topba5yPMxtvnRdDjCUtEegxnPwr85d+wq/wCmt25zMKP969sNWO+NeefA5fzh36jFr4JL+cH9RiC+TPY9GMzi4Wf7231snHe1KYcucNcQBWwAnUNN7Wa+bxrLzX4LN8uf2cawaabkmaeh0TB841p2E6Ul0PWcMzXgOY4OadhaQQfMQt15dyQyvq6CoDbDRfclrL2dYcg+EdXknXtALSQ4Tqoz1yMcWugp7jlbLKQQdYIOhrBFj60WMy6EKlmZ8zy08J80zx3tSmDPnHfx6UW/QnjJ9WkBdFgLgQohkvnHoa14iY8xyu8mOYAaR5mOHiuPRe6l6QAhCEACTYnUGOGWQbWRvePO1pP0JSkGP8Vn7GXccgDzrR0toKInXpRzzOJ2uc+csBPTotA9SVkraQaMVIOaB49lTIuJcsZ6nVS+UHFc3FBctCVJoYcVoSglaEqhoySmybCmk3a4tvtA2epOBctC5UsglFS1OVNTtjFm+snafOty5akrUlOw1ZZI2ui60ugFWgNlla3RdaIls0nHku5WPY9p5rOF/mumXKumEdRI1uwSSNHQA67QOgBwT3LsSLLxlpieeSX5i0JS1OarqRi6zdYQkZExycjFVSSxtAFRSjhad7QA5zLlzo3EeVY3sTrBcLbF6NzeY2a3D4J3m7y3RkPO5h0S712B9a855qT/AGxw5DC4H9eNXVmL1YfIzkZUytb0ANZ/ukBYqEISAEhxzi0/ZSbhS5Icc4tP2Um4UAUJiZsKfspfepUhLksxQ6qfs5vepU3ulA5L+uyhwbZvCaSAuWhcu9LEJA7WGuAu0G9nW2i/IbJG8kGx28qUoOOprGSehuSuZctC9alyQ7mxctCVglaEqkVc2JWt1glYuqC5m6LrW6LqkK50ui653Wbq0SzLzqWmcIeOzrz77VlxWM4XlM69R/qBKRhV1IghCEjImOarjp7J28xXXmO4lP6XNuxqlM1XHT2Tt5iuzMdxGb0ubuYkwLEQhCQAkOOcWn7KTcKXJDjnFp+yk3CgDz9jZ8Wn7Ob3qVNTk6Y5sp+zm96lTUVotBHaLYtJBpM0vhR2D+lhNmu9R8U9BYtoitWSFjtIDSGsPbs0mOFnt6Ljl5DY8iqrG8Ey6MrSa3EZejTWayDQOo3aRpRu+M3kPQRsI5CCk+muexvc6lywSueksaSaKUjcuWNJaFy1uqC500kaS53RdMLnXSRdc7rIKtCubkrvl+wWB5RNKAegucT3D2JNf6O9Ksvj4o7aTeepkY1NSFIQhIzJlmq46eydvMV15juIzelzdzFSmarjp7J28xXXmO4jN6XN3MSAsRCEJACQ47xafspNwpckOOj+zT9lJuFAHnvG3eLTH/1zWt6VKmpOeNNAZSgbBFKB5hVSWTXdax0EzowrqDrBtfnHOOUJMCuoctopNWZF2ndD7Jg7ZoQ6LWPik2N+g/BeOfYeXkIiFZTujNjfbbWLEEfBcPgno9lxrT7h+JugdpDW0+W3nHOOlc8fxONxDtHTY4WdawcW/F6w2g8h85vhKHB3X5fv77HWpKqnNZPr+/vjuR/TWdJaVkJjeWk6Q1FjhqD2OAcx46C0g25L25FyD1ArnfSWLrTSRdMq5vdZuud1m6pBc3BW11zui6YHUHZ5x3rtnB8pnaVG+1JgdY8470oy/wDKZ16jfapkZ1CJIQhIzJjmq46eydvMV2ZjuIzelzdzFSearjp7J28xXXmNP9in9Lm7mJAWKhCEgBNGV9UYqKoeBciJwt1ho/SndMWXX/j6nsz9CAKIyg8mm7Ob3qRNF07ZR7Kbs5vepUzXWsdCWb6SccDwuWrlEMABeQSA4hos0XOtNd1Ms0R/7nH2cu6tb2VyTq7NliXycf7VqQHNRiuscHFY7PwzdvsVlZNYzNC+TwqRz4Kirq4qeR5vwErKqWNkDifgPDRoHkcC3laEqpauQ4AJTI/hfudp8IXO09Pwa+np3vpX132rGpNzVmaU1wu6KunzUYo6GIGOLhIy9n45tjETps122tc6X1OHMkgzRYt8nD+2b9iuXI+jjFpAyta7g26Rq6iqkjdpWJLWySube422FrrMuJeB1s4qJD4PNAaqIvLiI3U7Q2pY2+xuhwTwBy8IVminJlNjNJi3ycP7Zv2Lb70uK/Jw/tm/YroyabMKEy1Dn8NOJKh7XOceC4W72Qtv5IY3QbYW1tJ5Uy5uKdr4aWZ4rjKadkj5Kioq3wSOdG3SIY6UtN9IkeL7E7hxMrL70uK/Jw/tm/Ym7HsgK+igdUVLIxG0tDiyQON3uDG6rc7grdy1rYY61vhT6psDKSSVwo31zQ1zZWDTf4ORYAE63agmvL8TjJ53hRJk04jdxY5+gatpiD3N8VzxHoBxFwTfWU7j4mUfdC5By2DlZodGnWPOO9LMv/KZ16jfakTTrHnHeluX3lM69RvtUyM5kSQhCRBMc1XHT2Tt5iuvMdxGb0ubuYqVzU8dPZO3mK38xB/A1Xbnm+M9JgWghCEgBMWXPEKnsz9CfUxZc8QqezP0IAoXKU6qbs5vepEyXTxlMdVP2c3vUiZNJaw0JZvdSfNvisNNXsmqZGxxtZIHPfewLm2A1dKit1NM0H/k4upLuLV6MXUsGPKfAxBLTOrY3xzPmkkDy6955XSusQ0Ws55sdosNdxdYblVggovAPui3g+A8G0ifwmgY+D0r6FtK3La1+RPmB43PVPc6NkHAsmlhkaZH+EM4J749NzQ0gEuZfQNvFcDfkW1ZilS6skpqdkFo4YpXOnMlzwr5m2AaOTgvnXO7dCxiw3K3DaZwL8afK0N0Wxzim0ANViDFA11wBz8qzlblBglUGRVdaxvBvZMA0uBc1zL6DvFOlG9j7EcocpRiklUyEPibTl7WF0wkMobcNvaOwva99vQk2TlfVVNKyoeynaZoWSwNaZSA57dICQkbAC3Z0qCnboIJs4+EOaWmvisQQbafKLfFTFgOUuFUgiYzHJHxRNDGQyil0NFrdFoJZTtfq1HyuTXdSPCsSrpKmWF8dKGwOiErmmclzZGB50ARtAPKnKjxMvrKimLABDFBI13KTMZgQR0cEPamSRuTLXBXVHhDq+Mu4F0BYblhY57Xm40bk3bbbaxOpRHLvKHDPuRJRUVaJjpxmCMklzIxOx/BsOiPEY0EC9yAALmysvHsSmjnp4KdkRdOJiXTl4DREGHVojWTpfMoxnd4X7izmcR6enD+J0tG3Dx28rXdAHnkOWwck4ct2uWlzUUxu1jzjvT1lyPwX+e/vkTDGdY8470/Zcfif893fIpZMyEoQhBBMs1PHT2Tt9it7MR+Jq+3O89VDmp46eydvsVvZiPxNX253npMC0UIQkAJpyspuFo6hl9G8T9dr7BpbL9Cdkhxzi0/ZSbhQB5wymOqm6k3vUiY7p5ynOqm7Ob3mRMektoaEs6XU1zPH/ukfZy7ig2ku9LVPjdpRSPjdsDonvY6x2gOaQVpa6sSehZsInmq4ZnUkVPJFLpS1UUoLpIWhwEQs0OeH3aC19g3WRcgLljuT5krpKiTD4qyN0EMcfCmnux0b53PsJAdokZs5lR7Mcq/zyq/ear+dZlxistqrKr95qv50v8AHdr3DmI9MzNfJAQWaL3xkFlwdFzm+TpDUbE2uk2StC+CipYJbB8UEUcgBuNJkbWuseUXBXmt+PVghv4ZVX5/CKi+8myiyxr4Zo5PC6l4Y4OLHzzlrgD4zSC47RqXFGd726HZVw7p2betj1FhWHyR1VZK4DQmdCYiCCToQhjrjk1hN8sFVDX1FRFTcMyaGnY20sbCHQmcuuHc/Ct9hVCZY4hXQyCaCtqzTTgSQuE9TZocL6B8bV0D1bQVHv8Aqmv/AD6q/eKj+Zaa5owas7HprK7BnVE1JIaSOpZEJuFhlMNg6RrA0jTFjYtKYM6jC3AZW+Dtpw10LWxMMZa1oqI7W0NQHQqE/wCqa/8APqr94qP5lyq8eq5Wlk1VUSMNrslmme02NxdrnEGxAKAsJAVu0rgCt2lO5qhVEdY847wnrLmbxWs55ZXfquI+v8yYoT4zfOO8J4y62x9effagmoRVCEJmZMs1PHT2Tt9it7MMw+D1R5DO4A+Zz794VQ5qeOnsnb7FdOYriM3pc31EgLGQhCQAkOOcWn7KTcKXJDjnFp+yk3CgDzVlSdVN1JveZEw3T7lPsp+pN7zImArWOhLNw5bArjdbBytMQqY5K49YKb2OSqJ+o9IXRTlfIiSNq6AtjAPxQfbrUamapzj8d2McNjmDuULlGsrw8NK6bZ9BjoK6S2HDB8oZYojA5jZoTc6EgPi31nQPICdZBBHQtC2il2GWB3KNHhWeq3jdyboZTG4OH/OhPrMThI0iwaXmC9WiouNm15/lZnh1E79fL+xOcnri8cpcOxlv7G6RTbVUD4xc6wNpbpar6hpBwDm+sBWvkXlZCymddo0w432a76x6lFMqsoNOobPoAsPiTxjUJIzqIPTbYeQhp5AuPnLmSg46aNX931O2eFtSjUT13t7bEKBWzSu+KUfAzPjB0mggxu+PG4B8b/8AExzT60nCs50zvAfGb1m94T3lztj68++ExQeU3rN7wn3LjbH1599qEE2RZCEKiCZZqeOnsnb7FdOYriM3pc31FS2anjp7J2+xXTmK4jN6XN9RICxkIQkAJDjnFp+yk3ClyQ45xafspNwoA815RC4p+pN7zImGRqf8d/IdSX3mRM0jV0Rj3bkN5iQoBWz2rkUgO7ClEZSNrkqhK0pvMTH5h4WjPxonWPVOsd5UOqBrUrwCUCXg3eTMNA9b4KjmLUxjkcw7QbLznT5dacd8156/f1R67qcyhCW2T9hA8Lg8WSkri8LWLOSrEV4VORq511xI3b6wkdH5ScMSZ+Dv5u9Q8pFxbdJmMRHCUtPNrJZp00h1fAPCRH9SQt80SagnjBG8LBVwcvBipiufh05u+3+U+Y/4QkOH0RlPlBrR5T3kADzc56Arj1WxgottW6nKnPjN6ze8J9y2/J9effakbBCXOY0eTZ0T/GBcWkFwcDyEXSzLb8n1599qrLoKaaeZF0IQmQTLNTx09k7fYrpzFcRm9Lm+oqWzU8dPZO32K6cxXEZvS5vqJAWMhCEgBIcc4tP2Um4UuSHHOLT9lJuFAHmvG/yHUl95kTW4Jyx4/iOpN7zIm267aXyWMpanCRqTPCXOauEkamcBpiUFKYHJO5q6xFRHJjYv121aiNYPSNiW5WQcKyKraPLGjJbke3U5IInKRYDEJ4pqV3wm8LD12+UB5xb2JYqm2lUXTX6PX8+Rvhqlm6b6+vT8eZAXBcSl1TCQbEbNXsSN7VzpWbRtUzsYhdYpzr3Xh9nemkFLHvvGQpms0wpPuyXgxTkjWGGsp5BySNa7qyfg3/wuKbKi+mbm5ubnZy7dWxLMDZeeLokYfY4FO1TgDTBJMJLFmgdFw8Zwf8LzX7kJrmP6elxqjKVHiW5H6ckOaf0re3UnzLX8n1599qZdMaTQPjN7wnnLP8n1599qtHPPWxGUIQqIJlmp46eydvsV05iuIzelzfUVLZqeOnsnb7FdOYriM3pc31EgLGQhCQAkOOcWn7KTcKXJDjvFp+yk3CgDzRjn5HqS+8SJrunfFWXMXUl95kTTKyy7aSfAmQ9TZq3dHdJmPS6F110JKRm8hDLCuIbZPrafSSeooCORY1KLjmUpCOJPOC1BjlY8bWm/2j2XCa2xEFOFFGbhdGHjfJmNWVs0dcuMKDJTIzyJRpt9e1Q6ZitutoxUUgBGtmzzHaq3r6IsJaeReXVXBUcHqj2Irm0VUQxuC6xnxSESsRGFDVzBZMcsm47zx9b6CVKc4mHmngoxbXJAWydZjtIf6pHqTNkTT6dXEP0julTzPHT/ANlpz8R9vUWG/wA4C46t41Yzv4fyelTzw3Bu2/4RTcHlt6ze8J/yz/J9effamSNvjN6w7wnvLLZF1599q7TyGrEZQhCYiZZqeOnsnb7FdOYriM3pc31FS2anjp7J2+xXTmK4jN6XN9RICxkIQkAJDjvFp+yk3ClyQ47xafspNwoA86TNuWdST3mRN1ZAnMnxmdnJ7zItahgK9jDQUqC8zCT7xGpG2XSCWyU1cCbzqWecGXa6H6jnCkFE1rhY2KhNPPZPmHV9l0QmYyiSCTAY3a26u5ax4MWJZQVgKdGOBWqlw6GEoSeTM4RHYFp2EKMZZYJonTA1HvUsi1FLsRoxPTHVrC8Hte8HGuujs/oz2ey6iinSloygquGxKTxtUgx2iLHkHnTS2JVCPFG6FWXBNpkkyAhJqmEfBBd7CP8AdTTOg/SpgP0mnv8AtTBmzg/DvdzM7z/snnOG+8QHSvGxVX46pf7J/Y9nDxTpx+j9yqOBsR5x3pfljsi68++1Ymi1ErOWGyLrz77V6cHc8WurMjSEIVmJMs1PHT2Tt9iunMVxGb0ub6ipbNTx09k7fYrpzFcRm9Lm+okBYyEISAEhx3i0/ZSbhS5Icd4tP2Um4UAecJz4zOpJ7xIgSIl8pnUk94kXJ4Xs4R2pLzIlG5mVoKaaynsnQOWJGAiy1qQU0KPdZHw6yVQTWWKulskoNlwqbg+GRrKF80SfDMRta5Uoo64HlVcRS2TxQYgQujiM+BMsOOdPeCT3JadhUFoMRvyqRYPU2eDdY4mCqUpRfVFUouM00MuX2Blt3tHSoC2BX5jVEJY7EcllTuJUBikcw8hXB2RU5kXTeqOntBOyqLzJBm3itwp8w9mv6VjL2TyW9P2pwzew+K89PcEjywju8eu3zfavExdNrtGTe/okezg38GP/AD6kHqY/FPmKSZX7IutPvtTtXw2Y7qnuTRldsi602+F6dF3R5WMhwzRHEIQtzjJlmp46eydvsV05iuIzelzfUVLZqeOnsnb7FdOYriM3pc31EgLGQhCQAkOO8Wn7KTcKXJDjvFp+yk3CgDzfKfHZ1JPeJFh64Vr7Oj6knvEiw2VerhZfDSHbI2cFgOQXLUrquKxmQApuqaXmThdYKwqwU0aRyGW1l1jelVRT8yQuaQuRSlTdmaOCeaHWlrCFJcHxTWNfLyqEMel1HU2K14rrII2TzL8pZw+MG/JrUCy4otF4eOXV6wl+SuM6UYaTsSzKyISU5I2jWPUvksBi5YftNwlpJ2PVrYZuk1uJM3rPEf60jysgOnfzfYu+Qc+jcc5KeMfo9PWF19sPl1uZ4muF7vCnpworfEofwMh5mOPzFRjK7ZF1pt8KwMYo7U855opD/AVX2Vh1R9abeaqwFXmRk/E4u1klUjbYjqEIXoHlEyzU8dPZO32K6cxXEZvS5vqKls1PHT2Tt9iunMXxGb0uf6iQFjIQhIASDHuKz9jJuOS9IccYTTTgbTFIB5yw2QB5fxN/4k88b/8AXeuDJVrjjtDwcH4szD52zvH0JKHrvw8u5YL2HJsq3003NkXVsq6lIdxZprOkknCIEqGykxUSuEsQKBIs6azlFMuMrCGSIhYY9LXC6TyQ8y5nFwd0a5SHzBcSLbWPqU2ocVEkbmE67cqq2F5aVI6CqNgQdYXz3amFTnzI6n1HZUo16XJnqtCQ5NvLZHDmdces7PmU+lZpAf8ANqrLCKjRmufhd97/APPMrNpX3YPMsMVKWJTW6+6MsdQ5LSXiR7KulDaOpP8A6ZNwql8qj5HQ6XvarfznYsyGkMVxwlQWxxt5dHSbwjrc2jcX53BU/lT5TOkykeqQs72FadiwnGlJyXX0PnsdPimvoMSELNl7RxExzU8dPZO32K6cxXEJTz1UxHmIYVS+Qp8Ghqq5w1NZwMP6UjiDYeYhl+gnmV7ZmcNMGFxBwsXudIb9J0fqpMCcIQhIAWCFlCAPMedvJ+Smnc3RPB6WnA7UdIOA0h7RpHpL+i8CZVvbq9mkF7Hx/Aaati4GribIza29w5rvjMcNbXdIKrfEMyERJ8GrJIxyNmjZMB0AgtPtuqUmtAKDGIu5m/P9qz90nczfn+1XU/MfNyVsRPTT2+uuZzH1H53Af8kj6VfOnuBTX3TdzN+f7Vn7qP5m/wAX2q4DmQqfzmn/AGbh9C0dmQquSel9bXj6iOdPcCo/uq/mb/F9qz9138zf4vtVsnMhV/LUvsk/prH3kKz5Wk/+n9JHOnuBU/3YfzN/i+1Z+7D/AIrf4vtVsDMhV/LUv/0/prIzIVXy1L7H/wBNHOnuO7KkOKu+K3+L7V3p8fkZsaz16X2q1xmPqfzimH+B5+qt25j6jlq6ceaFx+kLKaU1aRtSxNWk7wlZlXtyslBBDI9WseX9qdxnSrgzQY2BvM4Me5w6fGcR8ynjMx8/LWw26Kc/zJVT5j3X/CV40eaOnAd+sX/Qs40acXeKNauPxNX55tlORVVRV1Ilnc6WQa7v6LloHI1o26rAAFOVZhMsxaXRTeIwNs2B51XLiduslzy49Zehck83FDQOEjGulmGyWchzmnnY0ANaekC/SpgtDjPIoyVm5IKk7NlO/ltbl17QllPkJWPto0dUb7LxNb3u1L1chO4FFZM5p6ucxNrnGGnjJLY9JpfrNyGhupjiSfHNz81rxp4GxsaxjQ1rAGsaNga0WAHRYLohIAQhCABCEIAEIQgAQhCABCEIAEIQgAQhCABCEIAEIQgAQhCABCEIAEIQgAQhCAP/2Q==',
            "rating":5,
            'MerchantType':'Amazon',
            'uuid':uuid()
          },
          {
            "name": "Oneplus 7",
            "price": 39000,
            "image":'https://static.digit.in/product/thumb_130774_product_td_300.jpeg',
            "rating":5,
            'MerchantType':'Amazon',
            'uuid':uuid()
          },
        ]
        },{
          MerchantType:'Snapdeal',
          Products:[
            {
              "name": "iPhone 8",
              "price": 51000,
              "image":'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDw0PDRANDQ0NDg8PDQ0NDQ8NEA0NFRIWFxURFRUYHSkgGBonGxUVITEhJSkrLi4uFyszODMsNygvLisBCgoKDg0OGhANFisgHx8tKys3Mi0rLS0rMi03Ky0rNy0rKzctNys3KysrLSs3KzctLSstLSs4NzctKy0tLSsrLf/AABEIARMAtwMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwECAwQFBgj/xABBEAACAQIBBgkJBwQCAwAAAAAAAQIDBBEFBxIhNHMGMTJRYXKxstETFCRBcXSRkrQVFyJSVIGTQqGzwYKiI0NT/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFxEBAQEBAAAAAAAAAAAAAAAAAAERIf/aAAwDAQACEQMRAD8AnEAAAAALalSMVjJxiueTSRFXDDOJWqOpSybLyNtTbjK9STnXkng/I46owxTWlg3LjWCWLja5uataTlVk6sm8dOs3Xm/3niUfSrypa+uvb/zU/Ep9q2v6i3/mp+J81xi364/x0/AyxodX+On4AfR/2ra/qLf+an4j7Vtf1Fv/ADU/E+dFbLo+Sn4F3mi6Pkp+AH0T9q2v6i3/AJqfiPtW0/UW/wDNT8T5281XR8lPwHmq6Pkp+AH0T9q2v6i3/mp+I+1bX9Rb/wA1PxPnbzVdHyU/AeaLo+Sn4AfRP2rafqLf+an4j7VtP1Fv/NT8T5280XR8lPwHmkej5KfgB9FLKdq9Sr0G+ZVoeJswnGSxi1Jc6aaPmevbxjFybikli8adN/6OdZZcq0JqVGU6OD1ToTdCf/TBP2MD6rBGHAXOJUnOjb5SlGca7jC2vlFQ0qj1KnWS1Jt6lJYLHU0nrcnkAAAAAAAAA8nnNylKhk+cIScJ3c42ykm04wknKq01xPycZ4PnPWEd5356slwetTuai/d09HH4TYEW5clozhQiklSjHSiuLyrSx+GpLoRxL69jRnoYOrUjhpxUtGEHzN8bfwOvlOfpdWT4lXxfsTPKZVxpXVxGoljKU5RcuKUZ44SXx+KNRK7uTcpU6zVPR8lVfI/E5RqNLi18TOlTZ5TJKda6o+TSjhUjN6PJhCLTb6Fq/uepqxc3LQk4Yz0k0sfw444CkbVKDeqKcnzJNv4FzWGp6muNPVgzv8CsqW9tUq+X0Y6cVoVJRlJRwxbi8NevVr6DU4S3tGvczqUElBqKxUXHTkuOWD+H7GNu5jWccoAGkAAAANfKF0qUHL+riiueQHLy7d4vyUeKOufS/UjkFZSbbb1tvFvnZQDv8GpqpGvbT1xdOVWC5nFfjS9sf7pM+guAuVJ3eT7apVelWgpUa0nxyq0pODm/bo6X/I+d+CL9NoL82nF+xwZNeZytp2NzzRvqiS5v/DRb/u2B7wAEAAAAAAI+zr8vJe+rdkCQSPs6/LyXvq3ZACJMqr0ivvZ9pq16SqRUakadRR5PlI6TiuZNYNfE28q7RX3su0wxKFlQVNOMIwpxlylTjo6XtfG/3ZuQMMDNADNEuxMaZcBdiMS0AXYjEtAF2J5nKt35Wpq5ENUennZ08tXehDQi/wAU+PoiefAAFQOvwS262677rJrzO7Jee/v6W3IU4Jbdbdd91kvZmG8MqLF4KtbtRxeim6CxaXEm8F8FzASUACAAAAAAEfZ1+XkvfVuyBIJHmdea8pkqOK0vK1no4rHRwgscOYCKMq7RX3s+0wRM2Vdor72XaYIlGaJlizDEyxYGZMriWRZXEC7EYluIxAuxLatVQi5S1KKxYxONlq6xfk48UdcumXMBz7ms6k5Tlxt8XMvUjGAAKlCoHX4Jbdbdd91ky5m7dKjlCri8al3Cm46tFKFvSaa9eL8o/giGuCW3W3XfdZNOZya81vY4rSV9i44rFJ2tDBtfs/gB78AEAAAAAAI0zrJed5MfrwqrHo0oklkaZ1tryZ7KveiWCMMrbRX3s+010bGV9ouN7PtNdAZImSLMSL0wMqZdiY0yuIF+J1OD+R5XlRxx0KcEnUnhi1jxRS53g/gdrI3Bmj5OM7lSnOaUvJqThGmnxJ4a2zoUshUKflFCpXp0aiXlKUaujGSWPHLDSw1v1geCy/VpUJ1lRk5wjJwpSk03JrU3itTWOL9h5RtvW9bfG+kz5fytTuL2orSCjaU/wU/xN4panUxbfG/VzYGuAAAAqUKgdfglt1t133WTDmaX4Mpb+h/hRD3BLbrbrvusmHM1yMpb+h/hiBI4AIAAAAAAR1nWoR8tkupr09OtDjeGj+B8XPj6yRSP863LyXvqvZAQRLlfaLjez7TWRs5Y2i43s+01UUZEXpmJMvTAyJlcSxMASgqyaTXE1ivYeKzncJfN7fzWlLCvcp6bT106HE37XxfE2smZfpU7abryUfNoNvHjnTXJw53xL4ESZZvq13Xq3FRSxqSxWptQguKK6EgGSZpOUXxyww6cPUdM41hbuck3yYvFvnfMdkAAABUoVA6/BLbrbrvusmTM1Cn5HKEk06ru6anHSxapq3paD0fVrc9frw6CG+CW3W3XfdZMOZrkZR39D/CgJHABAAAAAACP863LyXvqvZAkAj/Oty8l7+r2QAiXLG03G9n2mojay0/Sbjez7TTTKMiLkzGmVxAyYlcTHiYrqvoRb9b1R9oGrlOvi9BcS5XSzRDYAAAAAABUADr8Etutuu+6yYczXIylv6H+GJD3BLbrbrvusmfM3Qire/qJfjneRhJ4vXGFtRcVhxLXOXxAkEAEAAAAAAI/zq8vJe/q9kCQCP8AOry8l7+r2QAiLLT9JuN7PtNPE2stv0q53s+00sSjJiEzHiVxAyaRzLqtpy6FqXiZ7urgtFcb4/YaQAAAAAAAAFQAB1+CW3W3XfdZNeZ3ZL339/S25CnBLbrbrvusmvM7sl77+/pbcD3oAIAAAAAAR/nV5eS9/V7IEgEf51uXkvf1eyAEO5cfpVzvp9po4m5l1+lXO+n2mhiUZMSkp4LF+osxMFeeOrm4/aBinJttv1lAAAAAAAAAAKgADr8Etutuu+6ya8zuyXvv7+ltyFOCW3W3XfdZNeZ3ZL339/S24HvQAQAAAAAAj/Oty8l7+r2QJAI/zrcvJe+q9kAIYy8/SrnfT7TQxNzLz9Lud9PtNDEorOeCNcunLEtAAAAAAAAAAACoAA6/BLbrbrvusmvM7sl77+/pbchTglt1t1n3WTXmd2S99/f0tuB70AEAAAAAAI/zrcrJe+q9kCQCP863KyXvqvZACE8vv0u630+050mb+X36Xdb6fac4oFAAAAAAAAAAAAAFQAOvwS2626z7rJrzO7Je+/v6W3IU4Jbdbdd91k15ndkvff39Lbge9ABAAAAAACP863KyXvqvZAkAj7Otysl76r3YAQjwgfpd1vp9pzzey9td1vp9polFAAAAAAAAAAAAAAqUKgdfglt1t133WTXmd2S99/f0tuQpwS262677rJrzO7Je+/v6W3A96ACAAAAAAEfZ1uVkvfVe7AkEj7Otysl76r3YAQfl7a7rfT7TRN7L213W+n2miUUAAAAtqJ4PDjArpLnXxKms6D1P1qXElhq0022/XqRVRqdPHqeKwS0njjz6sANgGroVMFrfFHS14vSwePrXrwNmOOCx48NftAqAABUoVA6/BLbrbrvusmvM7sl77+/pbchTglt1t1n3WTXmd2S99/f0tuB70AEAAAAAAI+zr8rJe+q92BIJH2dblZL31XuwAg/L213W+n2mib2Xtrut9PtNEooAABa5pc/wLgBbprp+DGmun4MaPS/iFHpb/cBp+34FVL2/BlMFzv4lwAAACpQqB1+CW3W3XfdZNeZ3ZL339/S25CnBLbrbrPusmvM7sl77+/pbcD3oAIAAAAAAR9nX5WS99V7sCQSPs6/KyXvq3dgBB+Xtrut9PtNE3svbXdb6faaBQAAAtnxFwAslxai6IwAGOWGvi/3+xlAAAAAVKFQOvwS262677rJrzO7Je+/v6W3IU4Jbdbdd91k15ndkvff39Lbge9ABAAAAAACO87c9GWStWONxUj7MYx1kiHgs71CXm9nWj/6bvRl0KpSnGP8A30F+4EGZe2u630+0551uFFPRvK7XJqNVIPnhJJpnJKAAAAAAUAAqChUAAAAAA7HBLbrbrPusmnMzLGyu3xY38tXst6C/0QzwSi/OdP1UaVSo/bo4Jfu2Tnmot3DJzm1h5xdV6kcfXGLVJP8AdU8f3A9kACAAAAAAHPy/kmne2te1qNxjWhoqcUnKnNa4VFj64ySa9h0AB808KMm1oSlRuIeTvbTVKP8ATVotvCcH64PW4v2xeDR5U+q8v8H7S/goXVJTcMXTqJunVpN+uE1rXs4n6yO8p5oJSk3b1qMov1XUW5/PTUcfgUQwCWPucufzWH8l14FPucuvz2Hz3XgBFBQlj7nLr89h8914D7nLr89h8914AROCWFmbuvz2Hz3XgV+5u5/+lj81yBEwJYeZu6/PYfPdeA+5y6/PYfPdeAEUAlf7m7v89h8914BZnLr89h8914ARQFr1LW3xIlj7nLr89h890/8AR0skZotCSdzWpqKfJtE4SfQ5zjJ/DADxHBLItavJWlsvSK+jKvVwxja0E+XLoWtpf1SwS1Js+hMm2VO2o0beitGlQpwp01zRisFj06jBkTItrZUvJWtKNGDeM2sZSqTww0pzeuT6WdAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/Z',
              "rating":5,
              'MerchantType':'Snapdeal',
              'uuid':uuid()
            },
            {
              "name": "Oneplus XR",
              "price": 65000,
              "image":'https://images-na.ssl-images-amazon.com/images/I/51YXG1bDM5L._SY445_.jpg',
              "rating":5,
              'MerchantType':'Snapdeal',
              'uuid':uuid()
            }, 
          ]
        }
      ],
    /*  products: 
      [
          {
              "name": "Redmi Note 7",
              "price": 15000,
              "image":'https://www.91-img.com/pictures/132510-v8-xiaomi-redmi-note-7-pro-mobile-phone-large-1.jpg',
              "rating":4,
              'MerchantType':'Flipkart',
               'uuid':uuid()
          },
          {
            "name": "Realme Note 6 pro",
            "price": 12000,
            "image":'https://www.91-img.com/pictures/129556-v10-xiaomi-redmi-note-6-pro-mobile-phone-large-1.jpg',
            "rating":4.5,
            'MerchantType':'Flipkart',
            'uuid':uuid()
          },
          {
            "name": "Realme pro",
            "price": 11500,
            "image":'https://www.91-img.com/pictures/129465-v6-realme-2-pro-mobile-phone-large-1.jpg',
            "rating":4,
            'MerchantType':'Flipkart',
            'uuid':uuid()
          },
          {
            "name": "Oneplus 6",
            "price": 35000,
            "image":'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEBIVFRUVFxcVEBUQFQ8WFRYVFRUXFhUVFRcYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0lHyUrKy8tLSstLS0tLTAtLy8tLS0tLS8tLS0tLS0vLS8tLS0tLS0tLS8tLS0tLS0tLS0rLf/AABEIARMAtwMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAABAUGBwECAwj/xABREAABAwIBBggIBw0HBQEAAAABAAIDBBEFBgcSITGyEzRBUWFyc7EUIjJxdIGRtEJSU6HBwtEVFyMzQ1RikpOUotLTJESCg7Ph8BYlNWOjw//EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAwEQACAQIEAwYGAgMAAAAAAAAAAQIDEQQhMVESE0EFImGBscEjMnGh0fCR4RRicv/aAAwDAQACEQMRAD8AvFCEIAE0YnlTQ050airgjd8V8sYd+re6rHOll9IS+lpD4ulwTnNc4XeCdMEt5tE6r2te99JujAKPDHhvjFrTtNmNO3oOpJtLUqMW9C+35xMKH98jPVEju5pXM5ysKH97HqiqT3MVIGhPx/4GLR1G75T+FqXHErlS2Lu++fhX5yf2FZ/TWjs6eFD+8P8AVTVp/wDzVIOo3fKn9Vq5mjd8qf1Go4kHKnsXj99XCvl5P3as/prH31sK+Wl/dqz+RUBXU0jPGuHDlOi2484skcZc69nDVsGiNey/J039RVKzIcWsmejPvq4V8tJ+7Vn9NZGdXCvl5P3at/przbCZHnRZt8zbAc5Nk5topba5yPMxtvnRdDjCUtEegxnPwr85d+wq/wCmt25zMKP969sNWO+NeefA5fzh36jFr4JL+cH9RiC+TPY9GMzi4Wf7231snHe1KYcucNcQBWwAnUNN7Wa+bxrLzX4LN8uf2cawaabkmaeh0TB841p2E6Ul0PWcMzXgOY4OadhaQQfMQt15dyQyvq6CoDbDRfclrL2dYcg+EdXknXtALSQ4Tqoz1yMcWugp7jlbLKQQdYIOhrBFj60WMy6EKlmZ8zy08J80zx3tSmDPnHfx6UW/QnjJ9WkBdFgLgQohkvnHoa14iY8xyu8mOYAaR5mOHiuPRe6l6QAhCEACTYnUGOGWQbWRvePO1pP0JSkGP8Vn7GXccgDzrR0toKInXpRzzOJ2uc+csBPTotA9SVkraQaMVIOaB49lTIuJcsZ6nVS+UHFc3FBctCVJoYcVoSglaEqhoySmybCmk3a4tvtA2epOBctC5UsglFS1OVNTtjFm+snafOty5akrUlOw1ZZI2ui60ugFWgNlla3RdaIls0nHku5WPY9p5rOF/mumXKumEdRI1uwSSNHQA67QOgBwT3LsSLLxlpieeSX5i0JS1OarqRi6zdYQkZExycjFVSSxtAFRSjhad7QA5zLlzo3EeVY3sTrBcLbF6NzeY2a3D4J3m7y3RkPO5h0S712B9a855qT/AGxw5DC4H9eNXVmL1YfIzkZUytb0ANZ/ukBYqEISAEhxzi0/ZSbhS5Icc4tP2Um4UAUJiZsKfspfepUhLksxQ6qfs5vepU3ulA5L+uyhwbZvCaSAuWhcu9LEJA7WGuAu0G9nW2i/IbJG8kGx28qUoOOprGSehuSuZctC9alyQ7mxctCVglaEqkVc2JWt1glYuqC5m6LrW6LqkK50ui653Wbq0SzLzqWmcIeOzrz77VlxWM4XlM69R/qBKRhV1IghCEjImOarjp7J28xXXmO4lP6XNuxqlM1XHT2Tt5iuzMdxGb0ubuYkwLEQhCQAkOOcWn7KTcKXJDjnFp+yk3CgDz9jZ8Wn7Ob3qVNTk6Y5sp+zm96lTUVotBHaLYtJBpM0vhR2D+lhNmu9R8U9BYtoitWSFjtIDSGsPbs0mOFnt6Ljl5DY8iqrG8Ey6MrSa3EZejTWayDQOo3aRpRu+M3kPQRsI5CCk+muexvc6lywSueksaSaKUjcuWNJaFy1uqC500kaS53RdMLnXSRdc7rIKtCubkrvl+wWB5RNKAegucT3D2JNf6O9Ksvj4o7aTeepkY1NSFIQhIzJlmq46eydvMV15juIzelzdzFSmarjp7J28xXXmO4jN6XN3MSAsRCEJACQ47xafspNwpckOOj+zT9lJuFAHnvG3eLTH/1zWt6VKmpOeNNAZSgbBFKB5hVSWTXdax0EzowrqDrBtfnHOOUJMCuoctopNWZF2ndD7Jg7ZoQ6LWPik2N+g/BeOfYeXkIiFZTujNjfbbWLEEfBcPgno9lxrT7h+JugdpDW0+W3nHOOlc8fxONxDtHTY4WdawcW/F6w2g8h85vhKHB3X5fv77HWpKqnNZPr+/vjuR/TWdJaVkJjeWk6Q1FjhqD2OAcx46C0g25L25FyD1ArnfSWLrTSRdMq5vdZuud1m6pBc3BW11zui6YHUHZ5x3rtnB8pnaVG+1JgdY8470oy/wDKZ16jfapkZ1CJIQhIzJjmq46eydvMV2ZjuIzelzdzFSearjp7J28xXXmNP9in9Lm7mJAWKhCEgBNGV9UYqKoeBciJwt1ho/SndMWXX/j6nsz9CAKIyg8mm7Ob3qRNF07ZR7Kbs5vepUzXWsdCWb6SccDwuWrlEMABeQSA4hos0XOtNd1Ms0R/7nH2cu6tb2VyTq7NliXycf7VqQHNRiuscHFY7PwzdvsVlZNYzNC+TwqRz4Kirq4qeR5vwErKqWNkDifgPDRoHkcC3laEqpauQ4AJTI/hfudp8IXO09Pwa+np3vpX132rGpNzVmaU1wu6KunzUYo6GIGOLhIy9n45tjETps122tc6X1OHMkgzRYt8nD+2b9iuXI+jjFpAyta7g26Rq6iqkjdpWJLWySube422FrrMuJeB1s4qJD4PNAaqIvLiI3U7Q2pY2+xuhwTwBy8IVminJlNjNJi3ycP7Zv2Lb70uK/Jw/tm/YroyabMKEy1Dn8NOJKh7XOceC4W72Qtv5IY3QbYW1tJ5Uy5uKdr4aWZ4rjKadkj5Kioq3wSOdG3SIY6UtN9IkeL7E7hxMrL70uK/Jw/tm/Ym7HsgK+igdUVLIxG0tDiyQON3uDG6rc7grdy1rYY61vhT6psDKSSVwo31zQ1zZWDTf4ORYAE63agmvL8TjJ53hRJk04jdxY5+gatpiD3N8VzxHoBxFwTfWU7j4mUfdC5By2DlZodGnWPOO9LMv/KZ16jfakTTrHnHeluX3lM69RvtUyM5kSQhCRBMc1XHT2Tt5iuvMdxGb0ubuYqVzU8dPZO3mK38xB/A1Xbnm+M9JgWghCEgBMWXPEKnsz9CfUxZc8QqezP0IAoXKU6qbs5vepEyXTxlMdVP2c3vUiZNJaw0JZvdSfNvisNNXsmqZGxxtZIHPfewLm2A1dKit1NM0H/k4upLuLV6MXUsGPKfAxBLTOrY3xzPmkkDy6955XSusQ0Ws55sdosNdxdYblVggovAPui3g+A8G0ifwmgY+D0r6FtK3La1+RPmB43PVPc6NkHAsmlhkaZH+EM4J749NzQ0gEuZfQNvFcDfkW1ZilS6skpqdkFo4YpXOnMlzwr5m2AaOTgvnXO7dCxiw3K3DaZwL8afK0N0Wxzim0ANViDFA11wBz8qzlblBglUGRVdaxvBvZMA0uBc1zL6DvFOlG9j7EcocpRiklUyEPibTl7WF0wkMobcNvaOwva99vQk2TlfVVNKyoeynaZoWSwNaZSA57dICQkbAC3Z0qCnboIJs4+EOaWmvisQQbafKLfFTFgOUuFUgiYzHJHxRNDGQyil0NFrdFoJZTtfq1HyuTXdSPCsSrpKmWF8dKGwOiErmmclzZGB50ARtAPKnKjxMvrKimLABDFBI13KTMZgQR0cEPamSRuTLXBXVHhDq+Mu4F0BYblhY57Xm40bk3bbbaxOpRHLvKHDPuRJRUVaJjpxmCMklzIxOx/BsOiPEY0EC9yAALmysvHsSmjnp4KdkRdOJiXTl4DREGHVojWTpfMoxnd4X7izmcR6enD+J0tG3Dx28rXdAHnkOWwck4ct2uWlzUUxu1jzjvT1lyPwX+e/vkTDGdY8470/Zcfif893fIpZMyEoQhBBMs1PHT2Tt9it7MR+Jq+3O89VDmp46eydvsVvZiPxNX253npMC0UIQkAJpyspuFo6hl9G8T9dr7BpbL9Cdkhxzi0/ZSbhQB5wymOqm6k3vUiY7p5ynOqm7Ob3mRMektoaEs6XU1zPH/ukfZy7ig2ku9LVPjdpRSPjdsDonvY6x2gOaQVpa6sSehZsInmq4ZnUkVPJFLpS1UUoLpIWhwEQs0OeH3aC19g3WRcgLljuT5krpKiTD4qyN0EMcfCmnux0b53PsJAdokZs5lR7Mcq/zyq/ear+dZlxistqrKr95qv50v8AHdr3DmI9MzNfJAQWaL3xkFlwdFzm+TpDUbE2uk2StC+CipYJbB8UEUcgBuNJkbWuseUXBXmt+PVghv4ZVX5/CKi+8myiyxr4Zo5PC6l4Y4OLHzzlrgD4zSC47RqXFGd726HZVw7p2betj1FhWHyR1VZK4DQmdCYiCCToQhjrjk1hN8sFVDX1FRFTcMyaGnY20sbCHQmcuuHc/Ct9hVCZY4hXQyCaCtqzTTgSQuE9TZocL6B8bV0D1bQVHv8Aqmv/AD6q/eKj+Zaa5owas7HprK7BnVE1JIaSOpZEJuFhlMNg6RrA0jTFjYtKYM6jC3AZW+Dtpw10LWxMMZa1oqI7W0NQHQqE/wCqa/8APqr94qP5lyq8eq5Wlk1VUSMNrslmme02NxdrnEGxAKAsJAVu0rgCt2lO5qhVEdY847wnrLmbxWs55ZXfquI+v8yYoT4zfOO8J4y62x9effagmoRVCEJmZMs1PHT2Tt9it7MMw+D1R5DO4A+Zz794VQ5qeOnsnb7FdOYriM3pc31EgLGQhCQAkOOcWn7KTcKXJDjnFp+yk3CgDzVlSdVN1JveZEw3T7lPsp+pN7zImArWOhLNw5bArjdbBytMQqY5K49YKb2OSqJ+o9IXRTlfIiSNq6AtjAPxQfbrUamapzj8d2McNjmDuULlGsrw8NK6bZ9BjoK6S2HDB8oZYojA5jZoTc6EgPi31nQPICdZBBHQtC2il2GWB3KNHhWeq3jdyboZTG4OH/OhPrMThI0iwaXmC9WiouNm15/lZnh1E79fL+xOcnri8cpcOxlv7G6RTbVUD4xc6wNpbpar6hpBwDm+sBWvkXlZCymddo0w432a76x6lFMqsoNOobPoAsPiTxjUJIzqIPTbYeQhp5AuPnLmSg46aNX931O2eFtSjUT13t7bEKBWzSu+KUfAzPjB0mggxu+PG4B8b/8AExzT60nCs50zvAfGb1m94T3lztj68++ExQeU3rN7wn3LjbH1599qEE2RZCEKiCZZqeOnsnb7FdOYriM3pc31FS2anjp7J2+xXTmK4jN6XN9RICxkIQkAJDjnFp+yk3ClyQ45xafspNwoA815RC4p+pN7zImGRqf8d/IdSX3mRM0jV0Rj3bkN5iQoBWz2rkUgO7ClEZSNrkqhK0pvMTH5h4WjPxonWPVOsd5UOqBrUrwCUCXg3eTMNA9b4KjmLUxjkcw7QbLznT5dacd8156/f1R67qcyhCW2T9hA8Lg8WSkri8LWLOSrEV4VORq511xI3b6wkdH5ScMSZ+Dv5u9Q8pFxbdJmMRHCUtPNrJZp00h1fAPCRH9SQt80SagnjBG8LBVwcvBipiufh05u+3+U+Y/4QkOH0RlPlBrR5T3kADzc56Arj1WxgottW6nKnPjN6ze8J9y2/J9effakbBCXOY0eTZ0T/GBcWkFwcDyEXSzLb8n1599qrLoKaaeZF0IQmQTLNTx09k7fYrpzFcRm9Lm+oqWzU8dPZO32K6cxXEZvS5vqJAWMhCEgBIcc4tP2Um4UuSHHOLT9lJuFAHmvG/yHUl95kTW4Jyx4/iOpN7zIm267aXyWMpanCRqTPCXOauEkamcBpiUFKYHJO5q6xFRHJjYv121aiNYPSNiW5WQcKyKraPLGjJbke3U5IInKRYDEJ4pqV3wm8LD12+UB5xb2JYqm2lUXTX6PX8+Rvhqlm6b6+vT8eZAXBcSl1TCQbEbNXsSN7VzpWbRtUzsYhdYpzr3Xh9nemkFLHvvGQpms0wpPuyXgxTkjWGGsp5BySNa7qyfg3/wuKbKi+mbm5ubnZy7dWxLMDZeeLokYfY4FO1TgDTBJMJLFmgdFw8Zwf8LzX7kJrmP6elxqjKVHiW5H6ckOaf0re3UnzLX8n1599qZdMaTQPjN7wnnLP8n1599qtHPPWxGUIQqIJlmp46eydvsV05iuIzelzfUVLZqeOnsnb7FdOYriM3pc31EgLGQhCQAkOOcWn7KTcKXJDjvFp+yk3CgDzRjn5HqS+8SJrunfFWXMXUl95kTTKyy7aSfAmQ9TZq3dHdJmPS6F110JKRm8hDLCuIbZPrafSSeooCORY1KLjmUpCOJPOC1BjlY8bWm/2j2XCa2xEFOFFGbhdGHjfJmNWVs0dcuMKDJTIzyJRpt9e1Q6ZitutoxUUgBGtmzzHaq3r6IsJaeReXVXBUcHqj2Irm0VUQxuC6xnxSESsRGFDVzBZMcsm47zx9b6CVKc4mHmngoxbXJAWydZjtIf6pHqTNkTT6dXEP0julTzPHT/ANlpz8R9vUWG/wA4C46t41Yzv4fyelTzw3Bu2/4RTcHlt6ze8J/yz/J9effamSNvjN6w7wnvLLZF1599q7TyGrEZQhCYiZZqeOnsnb7FdOYriM3pc31FS2anjp7J2+xXTmK4jN6XN9RICxkIQkAJDjvFp+yk3ClyQ47xafspNwoA86TNuWdST3mRN1ZAnMnxmdnJ7zItahgK9jDQUqC8zCT7xGpG2XSCWyU1cCbzqWecGXa6H6jnCkFE1rhY2KhNPPZPmHV9l0QmYyiSCTAY3a26u5ax4MWJZQVgKdGOBWqlw6GEoSeTM4RHYFp2EKMZZYJonTA1HvUsi1FLsRoxPTHVrC8Hte8HGuujs/oz2ey6iinSloygquGxKTxtUgx2iLHkHnTS2JVCPFG6FWXBNpkkyAhJqmEfBBd7CP8AdTTOg/SpgP0mnv8AtTBmzg/DvdzM7z/snnOG+8QHSvGxVX46pf7J/Y9nDxTpx+j9yqOBsR5x3pfljsi68++1Ymi1ErOWGyLrz77V6cHc8WurMjSEIVmJMs1PHT2Tt9iunMVxGb0ub6ipbNTx09k7fYrpzFcRm9Lm+okBYyEISAEhx3i0/ZSbhS5Icd4tP2Um4UAecJz4zOpJ7xIgSIl8pnUk94kXJ4Xs4R2pLzIlG5mVoKaaynsnQOWJGAiy1qQU0KPdZHw6yVQTWWKulskoNlwqbg+GRrKF80SfDMRta5Uoo64HlVcRS2TxQYgQujiM+BMsOOdPeCT3JadhUFoMRvyqRYPU2eDdY4mCqUpRfVFUouM00MuX2Blt3tHSoC2BX5jVEJY7EcllTuJUBikcw8hXB2RU5kXTeqOntBOyqLzJBm3itwp8w9mv6VjL2TyW9P2pwzew+K89PcEjywju8eu3zfavExdNrtGTe/okezg38GP/AD6kHqY/FPmKSZX7IutPvtTtXw2Y7qnuTRldsi602+F6dF3R5WMhwzRHEIQtzjJlmp46eydvsV05iuIzelzfUVLZqeOnsnb7FdOYriM3pc31EgLGQhCQAkOO8Wn7KTcKXJDjvFp+yk3CgDzfKfHZ1JPeJFh64Vr7Oj6knvEiw2VerhZfDSHbI2cFgOQXLUrquKxmQApuqaXmThdYKwqwU0aRyGW1l1jelVRT8yQuaQuRSlTdmaOCeaHWlrCFJcHxTWNfLyqEMel1HU2K14rrII2TzL8pZw+MG/JrUCy4otF4eOXV6wl+SuM6UYaTsSzKyISU5I2jWPUvksBi5YftNwlpJ2PVrYZuk1uJM3rPEf60jysgOnfzfYu+Qc+jcc5KeMfo9PWF19sPl1uZ4muF7vCnpworfEofwMh5mOPzFRjK7ZF1pt8KwMYo7U855opD/AVX2Vh1R9abeaqwFXmRk/E4u1klUjbYjqEIXoHlEyzU8dPZO32K6cxXEZvS5vqKls1PHT2Tt9iunMXxGb0uf6iQFjIQhIASDHuKz9jJuOS9IccYTTTgbTFIB5yw2QB5fxN/4k88b/8AXeuDJVrjjtDwcH4szD52zvH0JKHrvw8u5YL2HJsq3003NkXVsq6lIdxZprOkknCIEqGykxUSuEsQKBIs6azlFMuMrCGSIhYY9LXC6TyQ8y5nFwd0a5SHzBcSLbWPqU2ocVEkbmE67cqq2F5aVI6CqNgQdYXz3amFTnzI6n1HZUo16XJnqtCQ5NvLZHDmdces7PmU+lZpAf8ANqrLCKjRmufhd97/APPMrNpX3YPMsMVKWJTW6+6MsdQ5LSXiR7KulDaOpP8A6ZNwql8qj5HQ6XvarfznYsyGkMVxwlQWxxt5dHSbwjrc2jcX53BU/lT5TOkykeqQs72FadiwnGlJyXX0PnsdPimvoMSELNl7RxExzU8dPZO32K6cxXEJTz1UxHmIYVS+Qp8Ghqq5w1NZwMP6UjiDYeYhl+gnmV7ZmcNMGFxBwsXudIb9J0fqpMCcIQhIAWCFlCAPMedvJ+Smnc3RPB6WnA7UdIOA0h7RpHpL+i8CZVvbq9mkF7Hx/Aaati4GribIza29w5rvjMcNbXdIKrfEMyERJ8GrJIxyNmjZMB0AgtPtuqUmtAKDGIu5m/P9qz90nczfn+1XU/MfNyVsRPTT2+uuZzH1H53Af8kj6VfOnuBTX3TdzN+f7Vn7qP5m/wAX2q4DmQqfzmn/AGbh9C0dmQquSel9bXj6iOdPcCo/uq/mb/F9qz9138zf4vtVsnMhV/LUvsk/prH3kKz5Wk/+n9JHOnuBU/3YfzN/i+1Z+7D/AIrf4vtVsDMhV/LUv/0/prIzIVXy1L7H/wBNHOnuO7KkOKu+K3+L7V3p8fkZsaz16X2q1xmPqfzimH+B5+qt25j6jlq6ceaFx+kLKaU1aRtSxNWk7wlZlXtyslBBDI9WseX9qdxnSrgzQY2BvM4Me5w6fGcR8ynjMx8/LWw26Kc/zJVT5j3X/CV40eaOnAd+sX/Qs40acXeKNauPxNX55tlORVVRV1Ilnc6WQa7v6LloHI1o26rAAFOVZhMsxaXRTeIwNs2B51XLiduslzy49Zehck83FDQOEjGulmGyWchzmnnY0ANaekC/SpgtDjPIoyVm5IKk7NlO/ltbl17QllPkJWPto0dUb7LxNb3u1L1chO4FFZM5p6ucxNrnGGnjJLY9JpfrNyGhupjiSfHNz81rxp4GxsaxjQ1rAGsaNga0WAHRYLohIAQhCABCEIAEIQgAQhCABCEIAEIQgAQhCABCEIAEIQgAQhCABCEIAEIQgAQhCAP/2Q==',
            "rating":5,
            'MerchantType':'Amazon',
            'uuid':uuid()
          },
          {
            "name": "Oneplus 7",
            "price": 39000,
            "image":'https://static.digit.in/product/thumb_130774_product_td_300.jpeg',
            "rating":5,
            'MerchantType':'Amazon',
            'uuid':uuid()
          },
          {
            "name": "iPhone 8",
            "price": 51000,
            "image":'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDw0PDRANDQ0NDg8PDQ0NDQ8NEA0NFRIWFxURFRUYHSkgGBonGxUVITEhJSkrLi4uFyszODMsNygvLisBCgoKDg0OGhANFisgHx8tKys3Mi0rLS0rMi03Ky0rNy0rKzctNys3KysrLSs3KzctLSstLSs4NzctKy0tLSsrLf/AABEIARMAtwMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwECAwQFBgj/xABBEAACAQIBBgkJBwQCAwAAAAAAAQIDBBEFBxIhNHMGMTJRYXKxstETFCRBcXSRkrQVFyJSVIGTQqGzwYKiI0NT/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFxEBAQEBAAAAAAAAAAAAAAAAAAERIf/aAAwDAQACEQMRAD8AnEAAAAALalSMVjJxiueTSRFXDDOJWqOpSybLyNtTbjK9STnXkng/I46owxTWlg3LjWCWLja5uataTlVk6sm8dOs3Xm/3niUfSrypa+uvb/zU/Ep9q2v6i3/mp+J81xi364/x0/AyxodX+On4AfR/2ra/qLf+an4j7Vtf1Fv/ADU/E+dFbLo+Sn4F3mi6Pkp+AH0T9q2v6i3/AJqfiPtW0/UW/wDNT8T5281XR8lPwHmq6Pkp+AH0T9q2v6i3/mp+I+1bX9Rb/wA1PxPnbzVdHyU/AeaLo+Sn4AfRP2rafqLf+an4j7VtP1Fv/NT8T5280XR8lPwHmkej5KfgB9FLKdq9Sr0G+ZVoeJswnGSxi1Jc6aaPmevbxjFybikli8adN/6OdZZcq0JqVGU6OD1ToTdCf/TBP2MD6rBGHAXOJUnOjb5SlGca7jC2vlFQ0qj1KnWS1Jt6lJYLHU0nrcnkAAAAAAAAA8nnNylKhk+cIScJ3c42ykm04wknKq01xPycZ4PnPWEd5356slwetTuai/d09HH4TYEW5clozhQiklSjHSiuLyrSx+GpLoRxL69jRnoYOrUjhpxUtGEHzN8bfwOvlOfpdWT4lXxfsTPKZVxpXVxGoljKU5RcuKUZ44SXx+KNRK7uTcpU6zVPR8lVfI/E5RqNLi18TOlTZ5TJKda6o+TSjhUjN6PJhCLTb6Fq/uepqxc3LQk4Yz0k0sfw444CkbVKDeqKcnzJNv4FzWGp6muNPVgzv8CsqW9tUq+X0Y6cVoVJRlJRwxbi8NevVr6DU4S3tGvczqUElBqKxUXHTkuOWD+H7GNu5jWccoAGkAAAANfKF0qUHL+riiueQHLy7d4vyUeKOufS/UjkFZSbbb1tvFvnZQDv8GpqpGvbT1xdOVWC5nFfjS9sf7pM+guAuVJ3eT7apVelWgpUa0nxyq0pODm/bo6X/I+d+CL9NoL82nF+xwZNeZytp2NzzRvqiS5v/DRb/u2B7wAEAAAAAAI+zr8vJe+rdkCQSPs6/LyXvq3ZACJMqr0ivvZ9pq16SqRUakadRR5PlI6TiuZNYNfE28q7RX3su0wxKFlQVNOMIwpxlylTjo6XtfG/3ZuQMMDNADNEuxMaZcBdiMS0AXYjEtAF2J5nKt35Wpq5ENUennZ08tXehDQi/wAU+PoiefAAFQOvwS262677rJrzO7Jee/v6W3IU4Jbdbdd91kvZmG8MqLF4KtbtRxeim6CxaXEm8F8FzASUACAAAAAAEfZ1+XkvfVuyBIJHmdea8pkqOK0vK1no4rHRwgscOYCKMq7RX3s+0wRM2Vdor72XaYIlGaJlizDEyxYGZMriWRZXEC7EYluIxAuxLatVQi5S1KKxYxONlq6xfk48UdcumXMBz7ms6k5Tlxt8XMvUjGAAKlCoHX4Jbdbdd91ky5m7dKjlCri8al3Cm46tFKFvSaa9eL8o/giGuCW3W3XfdZNOZya81vY4rSV9i44rFJ2tDBtfs/gB78AEAAAAAAI0zrJed5MfrwqrHo0oklkaZ1tryZ7KveiWCMMrbRX3s+010bGV9ouN7PtNdAZImSLMSL0wMqZdiY0yuIF+J1OD+R5XlRxx0KcEnUnhi1jxRS53g/gdrI3Bmj5OM7lSnOaUvJqThGmnxJ4a2zoUshUKflFCpXp0aiXlKUaujGSWPHLDSw1v1geCy/VpUJ1lRk5wjJwpSk03JrU3itTWOL9h5RtvW9bfG+kz5fytTuL2orSCjaU/wU/xN4panUxbfG/VzYGuAAAAqUKgdfglt1t133WTDmaX4Mpb+h/hRD3BLbrbrvusmHM1yMpb+h/hiBI4AIAAAAAAR1nWoR8tkupr09OtDjeGj+B8XPj6yRSP863LyXvqvZAQRLlfaLjez7TWRs5Y2i43s+01UUZEXpmJMvTAyJlcSxMASgqyaTXE1ivYeKzncJfN7fzWlLCvcp6bT106HE37XxfE2smZfpU7abryUfNoNvHjnTXJw53xL4ESZZvq13Xq3FRSxqSxWptQguKK6EgGSZpOUXxyww6cPUdM41hbuck3yYvFvnfMdkAAABUoVA6/BLbrbrvusmTM1Cn5HKEk06ru6anHSxapq3paD0fVrc9frw6CG+CW3W3XfdZMOZrkZR39D/CgJHABAAAAAACP863LyXvqvZAkAj/Oty8l7+r2QAiXLG03G9n2mojay0/Sbjez7TTTKMiLkzGmVxAyYlcTHiYrqvoRb9b1R9oGrlOvi9BcS5XSzRDYAAAAAABUADr8Etutuu+6yYczXIylv6H+GJD3BLbrbrvusmfM3Qire/qJfjneRhJ4vXGFtRcVhxLXOXxAkEAEAAAAAAI/zq8vJe/q9kCQCP8AOry8l7+r2QAiLLT9JuN7PtNPE2stv0q53s+00sSjJiEzHiVxAyaRzLqtpy6FqXiZ7urgtFcb4/YaQAAAAAAAAFQAB1+CW3W3XfdZNeZ3ZL339/S25CnBLbrbrvusmvM7sl77+/pbcD3oAIAAAAAAR/nV5eS9/V7IEgEf51uXkvf1eyAEO5cfpVzvp9po4m5l1+lXO+n2mhiUZMSkp4LF+osxMFeeOrm4/aBinJttv1lAAAAAAAAAAKgADr8Etutuu+6ya8zuyXvv7+ltyFOCW3W3XfdZNeZ3ZL339/S24HvQAQAAAAAAj/Oty8l7+r2QJAI/zrcvJe+q9kAIYy8/SrnfT7TQxNzLz9Lud9PtNDEorOeCNcunLEtAAAAAAAAAAACoAA6/BLbrbrvusmvM7sl77+/pbchTglt1t1n3WTXmd2S99/f0tuB70AEAAAAAAI/zrcrJe+q9kCQCP863KyXvqvZACE8vv0u630+050mb+X36Xdb6fac4oFAAAAAAAAAAAAAFQAOvwS2626z7rJrzO7Je+/v6W3IU4Jbdbdd91k15ndkvff39Lbge9ABAAAAAACP863KyXvqvZAkAj7Otysl76r3YAQjwgfpd1vp9pzzey9td1vp9polFAAAAAAAAAAAAAAqUKgdfglt1t133WTXmd2S99/f0tuQpwS262677rJrzO7Je+/v6W3A96ACAAAAAAEfZ1uVkvfVe7AkEj7Otysl76r3YAQfl7a7rfT7TRN7L213W+n2miUUAAAAtqJ4PDjArpLnXxKms6D1P1qXElhq0022/XqRVRqdPHqeKwS0njjz6sANgGroVMFrfFHS14vSwePrXrwNmOOCx48NftAqAABUoVA6/BLbrbrvusmvM7sl77+/pbchTglt1t1n3WTXmd2S99/f0tuB70AEAAAAAAI+zr8rJe+q92BIJH2dblZL31XuwAg/L213W+n2mib2Xtrut9PtNEooAABa5pc/wLgBbprp+DGmun4MaPS/iFHpb/cBp+34FVL2/BlMFzv4lwAAACpQqB1+CW3W3XfdZNeZ3ZL339/S25CnBLbrbrPusmvM7sl77+/pbcD3oAIAAAAAAR9nX5WS99V7sCQSPs6/KyXvq3dgBB+Xtrut9PtNE3svbXdb6faaBQAAAtnxFwAslxai6IwAGOWGvi/3+xlAAAAAVKFQOvwS262677rJrzO7Je+/v6W3IU4Jbdbdd91k15ndkvff39Lbge9ABAAAAAACO87c9GWStWONxUj7MYx1kiHgs71CXm9nWj/6bvRl0KpSnGP8A30F+4EGZe2u630+0551uFFPRvK7XJqNVIPnhJJpnJKAAAAAAUAAqChUAAAAAA7HBLbrbrPusmnMzLGyu3xY38tXst6C/0QzwSi/OdP1UaVSo/bo4Jfu2Tnmot3DJzm1h5xdV6kcfXGLVJP8AdU8f3A9kACAAAAAAHPy/kmne2te1qNxjWhoqcUnKnNa4VFj64ySa9h0AB808KMm1oSlRuIeTvbTVKP8ATVotvCcH64PW4v2xeDR5U+q8v8H7S/goXVJTcMXTqJunVpN+uE1rXs4n6yO8p5oJSk3b1qMov1XUW5/PTUcfgUQwCWPucufzWH8l14FPucuvz2Hz3XgBFBQlj7nLr89h8914D7nLr89h8914AROCWFmbuvz2Hz3XgV+5u5/+lj81yBEwJYeZu6/PYfPdeA+5y6/PYfPdeAEUAlf7m7v89h8914BZnLr89h8914ARQFr1LW3xIlj7nLr89h890/8AR0skZotCSdzWpqKfJtE4SfQ5zjJ/DADxHBLItavJWlsvSK+jKvVwxja0E+XLoWtpf1SwS1Js+hMm2VO2o0beitGlQpwp01zRisFj06jBkTItrZUvJWtKNGDeM2sZSqTww0pzeuT6WdAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/Z',
            "rating":5,
            'MerchantType':'Snapdeal',
            'uuid':uuid()
          },
          {
            "name": "Oneplus XR",
            "price": 65000,
            "image":'https://images-na.ssl-images-amazon.com/images/I/51YXG1bDM5L._SY445_.jpg',
            "rating":5,
            'MerchantType':'Snapdeal',
            'uuid':uuid()
          },
      ] */
     }
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'PRODUCTS',
      headerRight: (
        <TouchableOpacity onPress={()=>{rootAccess.reRenderAsMerchantType()}} style={{height:16,width:16,marginRight:20}} >
        <Image
        source={require('../assets/link5-128.png')}
        style={{height:16,width:16,}}
          title="Info"
          color="#fff"
        />
        </TouchableOpacity>)
    };
  };
 
  update_Layout = (index) => {

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    const array = [...this.state.products];

    array[index]['expanded'] = !array[index]['expanded'];

    this.setState(() => {
        return {
            AccordionData: array
        }
    });
}

   reRenderAsMerchantType =()=>{
   //  alert("hello")
   if(this.state.rendertype === 'products'){
    this.setState({rendertype:'Merchant'})
   }
   else{
    this.setState({rendertype:'products'})
   }
     
   }



       FlastListrender(items){
       
      //  if(!item.image){
      //    item.image='http://www.tttcdhanbad.org/images/NoImage.png'
     //   }
        return(
          <View>
            {
                        items.Products.map((item, key) =>
                            (
                              <View style={{flex:1,height:80,borderBottomColor:'black',borderBottomWidth:0,width:devwidth,justifyContent:'center'}}>
                              <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                                 <Image 
                                      style={{width: 40, height: 60,borderRadius:5,margin:20}}
                                      source={{uri: item.image}}
                                     />
                                 <View style={{ justifyContent:'center'}}>
                                  <Text style={{fontSize:18,fontWeight:'400',color:'#9415d8'}}>{item.name}</Text>
                                  <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                                     <Text style={{fontSize:15,color:'#bcb5bf'}}>Rs.{ item.price}</Text>
                                     <View >
                                     <Rating
                                     imageSize={18}
                                     readonly={true}
                                     startingValue={item.rating}
                            onFinishRating={this.ratingCompleted}
                            style={{ paddingVertical: 10,marginLeft:15 }}
                          />
                                     </View>
                             
                                  </View>
                                 </View>
                          
                                 <View style={{alignItems:'flex-end',right:10,position:'absolute',}}>
                                                              <TouchableOpacity onPress={()=>{this.deletethisitem(item.uuid);}} style={{height:25,width:25,}} >
                                                                  <Image source={require('../assets/del_icon.jpg')} style={{height:'100%',width:'100%'}}/>
                                                              </TouchableOpacity>
                                                          </View>
                              </View>
                            </View>                            ))
                    }
          </View>

        )
      }


      addNewProduct=(newItem) =>{
          var products =this.state.products;
          var alreadyExist=false;
           for(var i=0;i<products.length;i++){
            if(products[i].MerchantType === newItem.MerchantType){
              products[i].Products.push(newItem);
              alreadyExist =true;
            }
          
           }
             if(alreadyExist){
              this.setState({products:products})
             }
             else{
               var Products=[]
                Products.push(newItem);
               var NewProduct={
                 'MerchantType':newItem.MerchantType,
                 'Products':Products
               }
               products.push(NewProduct);
               this.setState({products:products})
             }

   /*  this.setState(prevState => ({
          products: [...prevState.products, products]
        }))  */
        }

      deletethisitem=(id)=>{

        var products =this.state.products;
        var alreadyExist=false;
         for(var i=0;i<products.length;i++){
           for(var j=0;j<products[i].Products.length;j++){
             if(id ===  products[i].Products[j].uuid)
             products[i].Products.splice(j,1);
           }
         }
         this.setState({products:products})
         /*   this.setState( state =>({
             products: state.products.filter(item => item.uuid !== id)
         }));  */
               }





        render() {
          if(this.state.rendertype==='Merchant'){
            return(
              <View style={styles.MainContainer}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 10, paddingVertical: 5 }}>
                    {
                        this.state.products.map((item, key) =>
                            (
                                <Expandable_ListView key={item.MerchantType} deletethisitem={this.deletethisitem} onClickFunction={this.update_Layout.bind(this, key)} item={item} state={this.state} />
                            ))
                    }
                </ScrollView>
              </View>
            )
          }
            return (
              <View style={styles.container}>
                <ScrollView>
                  
               <FlatList
                  data={this.state.products}
                  showsVerticalScrollIndicator={false}
                  extraData={this.state}
                  renderItem={({item}) =>
                   this.FlastListrender(item)
                  }
                  keyExtractor={item => item.email}
                />  
                </ScrollView>
                        <ActionButton onPress={()=>{this.refs.productmodal.openModal()}} buttonColor="rgba(231,76,60,1)"/>
                        <AddProductModal ref="productmodal" addNewProduct={this.addNewProduct}/>           
              </View>
            );
          }
        
}




class Expandable_ListView extends Component {

  constructor() {

      super();

      this.state = {
          PickerValueHolder : '',
          layout_Height: 0

      }
  }

  componentWillReceiveProps(nextProps) {
      if (nextProps.item.expanded) {
          this.setState(() => {
              return {
                  layout_Height: null
              }
          });
      }
      else {
          this.setState(() => {
              return {
                  layout_Height: 0
              }
          });
      }
  }

  shouldComponentUpdate(nextProps, nextState) {
     // if (this.state.layout_Height !== nextState.layout_Height) {
          return true;
     // }
    //  return false;
  }

  show_Selected_Category = (item) => {

      // Write your code here which you want to execute on sub category selection.
      Alert.alert(item);

  }

  render() {

      return (
          <View style={styles.Panel_Holder}>

              <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction} style={styles.category_View}>
                  <Text style={styles.category_Text}>{this.props.item.MerchantType} </Text>
                  <Image
                      source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
                      style={styles.iconStyle} />
              </TouchableOpacity>

      
              <View style={{ height: this.state.layout_Height,width:devwidth, overflow: 'hidden' }}>
                    {this.props.item.Products.map((item, key) => (
                    <View style={{flex:1,height:80,borderBottomColor:'black',borderBottomWidth:0,width:devwidth,justifyContent:'center'}}>
                    <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                       <Image 
                            style={{width: 40, height: 60,borderRadius:5,margin:20}}
                            source={{uri: item.image}}
                           />
                       <View style={{ justifyContent:'center'}}>
                        <Text style={{fontSize:18,fontWeight:'400',color:'#9415d8'}}>{item.name}</Text>
                        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                           <Text style={{fontSize:15,color:'#bcb5bf'}}>Rs.{ item.price}</Text>
                           <View >
                           <Rating
                           imageSize={18}
                           readonly={true}
                           startingValue={item.rating}
                  onFinishRating={this.ratingCompleted}
                  style={{ paddingVertical: 10,marginLeft:15 }}
                />
                           </View>
                   
                        </View>
                       </View>
                
                       <View style={{alignItems:'flex-end',right:20,position:'absolute',}}>
                                                    <TouchableOpacity onPress={()=>{this.props.deletethisitem(item.uuid);}} style={{height:25,width:25,}} >
                                                        <Image source={require('../assets/del_icon.jpg')} style={{height:'100%',width:'100%'}}/>
                                                    </TouchableOpacity>
                                                </View>
                    </View>
                  </View>                      
                        ))
                    }

                </View>


          </View>

      );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    backgroundColor: '#F5FCFF',
},
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    flatview: {
      justifyContent: 'center',
      paddingTop: 30,
      borderRadius: 2,
    },
    actionButtonIcon: {
      fontSize: 20,
      height: 18,
      color: 'white',
  },
  contentTitle: {
      flexDirection:"column",
      fontSize: 15,
      marginTop:7,
      marginLeft:4,
      width:'30%',
      fontWeight: 'bold'
  },
  textStyle: {
      //width:170,
      margin:0,
      //height:30,
      width:'70%',
      fontSize: 15,
  },
  content: {
      backgroundColor: 'white',
      padding: 22,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  iconStyle: {

      width: 30,
      height: 30,
      justifyContent: 'flex-end',
      alignItems: 'center',
      tintColor: '#fff'

  },

  sub_Category_Text: {
      fontSize: 18,
      color: '#000',
      padding: 10
  },

  category_Text: {
      textAlign: 'left',
      color: '#fff',
      fontSize: 21,
      padding: 10
  },

  category_View: {
      marginVertical: 5,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: '#0091EA'
  },

  Btn: {
      padding: 10,
      backgroundColor: '#FF6F00'
  }

  });
  